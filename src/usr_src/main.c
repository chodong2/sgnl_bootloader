#include "main.h"
#include "global_define.h"

bitflag_struct_t g_str_bitflag;
bt_uart_struct_t g_str_bt;

timer_struct_t g_str_timer;

batt_struct_t g_str_batt;
chg_struct_t g_str_chg;

void peripheral_init(void)
{


	//pwm_init();
	uart_init();
	
	key_init();
	
	pin_enable_init();
	
	monitor_init();

}

void struct_init(void)
{
	memset(&g_str_bt, 0, sizeof(g_str_bt));

	g_str_bt.ui16_tx_cnt = BT_UART_BUFFERSIZE;
	g_str_bt.ui16_batt_voltage = 3000;
	
	g_str_batt.ui16_batt_status = BATT_STATUS_INITIAL;
	g_str_batt.ui16_batt_level = BATT_LEVEL_INITIAL;
	
	g_str_chg.ui16_chg_status = CHG_STATUS_INITIAL;
	g_str_chg.ui16_chg_type = CHG_TYPE_INITIAL;
	
	g_str_timer.ui16_pwr_down_cnt = 0;
	g_str_timer.ui16_power_cnt = 0;
	
	if( POWER_KEY_STATUS )
	{
		g_str_bitflag.b1_power_key_flag = 1;
	}
	else
	{
		jump_app();
	}
}

void main()
{
	/* pefipheral configuration initialize */
	peripheral_init();
	struct_init();
	
	//power enable pin hold
	POWER_HOLD_ENABLE;

	while(1)
	{
		BT_CommandDecodeMain();
		
		if( g_str_bitflag.b1_power_on )
		{
			jump_app();
		}
		
		if( g_str_timer.ui16_pwr_down_cnt > 500 )
		{
			break;
		}
	}
	
	POWER_HOLD_DISABLE;
}
