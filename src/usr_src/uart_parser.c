#include "global_define.h"

typedef enum {
	RX_DECODE_CMD_SYNC_AA,
	RX_DECODE_CMD_SYNC_00,
	RX_DECODE_CMD_LENGTH,
	RX_DECODE_CMD_DATA,
	RX_DECODE_CMD_CHECKSUM
} RX_DECODE_MODE;

//event ID
enum            
{
  ACK = 0x00,
  DEVICE_STATE = 0x01,
  CALL_STATUS = 0x02,
  CALL_ID = 0x03,
  SMS_INDICATION = 0x04,
  MISS_CALL_INDICATION = 0x05,
  PHONE_MAX_BATTERY_LEVEL = 0x06,
  PHONE_BATTERY_LEVEL = 0x07,
  PHONE_ROAMING_STATUS = 0x08,
  PHONE_MAX_SIGNAL_STRENGTH = 0x09,
  PHONE_SIGNAL_STRENGTH = 0x0A,
  PHONE_SERVICE_STATUS = 0x0B,
  BATTERY_LEVEL = 0x0C,
  CHARGER_STATUS = 0x0D,
  RESET_TO_DEFAULT = 0x0E,
  VOLUME_LEVEL = 0x0F,
  EQ_MODE = 0x10,
  MISS_CALL_HISTORY = 0x11,
  RECEIVED_CALL_HISTORY = 0x12,
  DIALED_CALL_HISTORY = 0x13,
  COMBINE_CALL_HISTORY = 0x14,
  PHONE_BOOK = 0x15,
  ACCESS_FINISH = 0x16,
  REMOTE_DEVICE_NAME = 0x17,
  UART_VERSION = 0x18,
  CALL_LIST_REPORT = 0x19,
  AVRCP_SPEC_RSP = 0x1A,
  BTM_UTILITY_REQ = 0x1B,
  VENDOR_AT_CMD_RSP = 0x1C,
  UNKNOW_AT_RESULT = 0x1D,
  REPORT_LINK_STATUS = 0x1E,
  REPORT_PAIRING_RECORD = 0x1F,
  REPORT_LOCAL_BD_ADDR = 0x20,
  REPORT_LOCAL_DEVICE_NAME = 0x21,
  REPORT_SPP_DATA = 0x22,
  REPORT_LINK_BACK_STATUS = 0x23,
  RINGTONE_FINISH_INDICATION = 0x24,
  USER_CONFIRM_SSP_REQ = 0x25,
  REPORT_AVRCP_VOL_CTRL = 0x26,
  REPORT_INPUT_SIGNAL_LEVEL = 0x27,
  REPORT_IAP_INFO = 0x28,
  REPORT_AVRCP_ABS_VOL_CTRL = 0x29,
  REPORT_VOICE_PROMPT_STATUS = 0x2A,
  REPORT_MAP_DATA = 0x2B,
  SECURITY_BONDLING_RES = 0x2C,
  REPORT_TYPE_CODEC = 0x2D,
  REPORT_TYPE_BTM_SETTING = 0x2E,
  REPORT_MCU_UPDATE_REPLY = 0x2F,
  REPORT_BTM_INITIAL_STATUS = 0x30,
#if 1 //SH_ADD
  REPORT_LE_ANCS_SERVICE_EVENT = 0x31,
#endif
  REPORT_LE_EVENT = 0x32,
  REPORT_nSPK_STATUS = 0x33,
  REPORT_nSPK_VENDOR_EVENT = 0x34,
  REPORT_CUSTOMER_GATT_ATTRIBUTE_DATA = 0x39,
  REPORT_LINK_MODE = 0x3A
};

#define BT_CMD_SIZE_MAX				200

/*======================*/
/*  external variables  */
/*======================*/
uint8_t  BT_CmdDecodedFlag;
uint8_t  BT_CmdBuffer[BT_CMD_SIZE_MAX];
uint8_t  BT_linkIndex = 0;

/*======================================*/
/*  internal variables          */
/*======================================*/
static RX_DECODE_MODE  BT_CmdDecodeState;
static uint8_t  BT_CmdDecodeCmdLength;
static uint8_t  BT_CmdDecodeChecksum;			
static uint8_t  BT_CmdDecodeDataCnt;                    //temporary variable in decoding
static unsigned short BT_CmdBufferPt;                    //

void BT_CommandDecodeMain(void)
{
	BT_CommandHandler();
	if(BT_CmdDecodedFlag)
	{
            BT_CommandDecode();
            BT_CmdDecodedFlag = 0;
	}
}

void BT_CommandHandler(void)
{
    uint8_t current_byte;

	while (g_str_bt.ui16_rx_cnt)
	{
        current_byte = uart_read_byte();

        switch (BT_CmdDecodeState) {
            case RX_DECODE_CMD_SYNC_AA:
                if (current_byte == 0xaa)
                    BT_CmdDecodeState = RX_DECODE_CMD_SYNC_00;
                break;

            case RX_DECODE_CMD_SYNC_00:
                if (current_byte == 0x00)
                    BT_CmdDecodeState = RX_DECODE_CMD_LENGTH;
                else
                    BT_CmdDecodeState = RX_DECODE_CMD_SYNC_AA;
                break;

            case RX_DECODE_CMD_LENGTH:
                BT_CmdDecodedFlag = 0; //command receive flag clear
                BT_CmdBufferPt = 0; //buffer reset for command parameter
                BT_CmdDecodeCmdLength = current_byte;
                BT_CmdDecodeChecksum = current_byte; //checksum calculation start!
                BT_CmdDecodeDataCnt = current_byte; //save bytes number, use to check where is command end
                BT_CmdDecodeState = RX_DECODE_CMD_DATA; //next state
                break;

            case RX_DECODE_CMD_DATA:
                BT_CmdDecodeChecksum += current_byte;
                BT_CmdDecodeDataCnt--;
                BT_CmdBuffer[BT_CmdBufferPt++] = current_byte;
                if (BT_CmdDecodeDataCnt == 0) //no data remained?
                    BT_CmdDecodeState = RX_DECODE_CMD_CHECKSUM; //yes, next mode: checksum
                break;

            case RX_DECODE_CMD_CHECKSUM:
                if ((uint8_t) (BT_CmdDecodeChecksum + current_byte) == 0) {
                    BT_CmdDecodedFlag = 1;
                } else {
                }
                BT_CmdDecodeState = RX_DECODE_CMD_SYNC_AA;
                break;
            default:
                break;
        }

        if (BT_CmdDecodedFlag) {
            break;
        }
    }
}


void BT_CommandDecode( void )
{
    switch(BT_CmdBuffer[0])
    {
#if 1 //SH_ADD
		case BATTERY_LEVEL :
			
			g_str_batt.ui16_batt_status = BT_CmdBuffer[1];
			g_str_batt.ui16_batt_level = BT_CmdBuffer[2];
			
			break;  
		
		case CHARGER_STATUS :
	
			g_str_chg.ui16_chg_status = BT_CmdBuffer[1];
			g_str_chg.ui16_chg_type = BT_CmdBuffer[2];
			break;  
#endif

		default:
            break;
    }
}