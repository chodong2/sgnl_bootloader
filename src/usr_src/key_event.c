#include "global_define.h"

void key_init( void )
{
	key_config_power();
	key_config_vol();
}

void key_config_power( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(KEY_GPIO_POWER_RCC, ENABLE);

	/* Configure PA1 in input mode */
	GPIO_InitStructure.GPIO_Pin = KEY_GPIO_POWER_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(KEY_GPIO_POWER_PORT, &GPIO_InitStructure);

	/* for use ext interrupt */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Tell system that you will use PA1 for EXTI_Line1 */
	SYSCFG_EXTILineConfig(KEY_EXTI_POWER_PORT, KEY_EXTI_POWER_PIN);

	/* PA1 is connected to EXTI_Line1 */
	EXTI_InitStructure.EXTI_Line = KEY_EXTI_POWER_LINE;
	/* Enable interrupt */
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	/* Interrupt mode */
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	/* Triggers on rising and falling edge */
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	/* Add to EXTI */
	EXTI_Init(&EXTI_InitStructure);

	/* EXTI1 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = KEY_EXTI_POWER_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/* vol key / wake up pin Input Configuration
*  wake up	event	EXT13  PC13
*  vol down	event	EXT14  PC14
*  vol up	event	EXT15  PC15
*/
void key_config_vol( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(KEY_GPIO_VOL_RCC, ENABLE);

	/* Configure PC13 in input mode */
	GPIO_InitStructure.GPIO_Pin = KEY_GPIO_VOLUP_PIN | KEY_GPIO_VOLDN_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(KEY_GPIO_VOL_PORT, &GPIO_InitStructure);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Tell system that you will use PC13 for EXTI_Line15_10 */
	SYSCFG_EXTILineConfig(KEY_EXTI_VOL_PORT, KEY_EXTI_VOLUP_PIN);

	/* PC13 is connected to EXTI_Line15_10 */
	EXTI_InitStructure.EXTI_Line = KEY_EXTI_VOLUP_LINE;
	/* Enable interrupt */
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	/* Interrupt mode */
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	/* Triggers on rising and falling edge */
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	/* Add to EXTI */
	EXTI_Init(&EXTI_InitStructure);
	
	SYSCFG_EXTILineConfig(KEY_EXTI_VOL_PORT, KEY_EXTI_VOLDN_PIN);

	/* PC13 is connected to EXTI_Line15_10 */
	EXTI_InitStructure.EXTI_Line = KEY_EXTI_VOLDN_LINE;
	EXTI_Init(&EXTI_InitStructure);

	/* EXTI13 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = KEY_EXTI_VOLUP_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	NVIC_InitStructure.NVIC_IRQChannel = KEY_EXTI_VOLDN_IRQ;
	NVIC_Init(&NVIC_InitStructure);

}


void key_power_isr( void )
{
	if (EXTI_GetITStatus(KEY_EXTI_POWER_LINE) == SET)
	{
		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(KEY_EXTI_POWER_LINE);

		if( POWER_KEY_STATUS )
		{
			g_str_bitflag.b1_power_key_flag = 1;
			g_str_timer.ui16_power_cnt = 0;
			g_str_timer.ui16_pwr_down_cnt = 0;
		}
		else
		{
			g_str_bitflag.b1_power_key_flag = 0;
		}
	}
}

void key_volup_isr( void )
{
	if (EXTI_GetITStatus(KEY_EXTI_VOLUP_LINE) == SET)
	{
		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(KEY_EXTI_VOLUP_LINE);

		if( VOLUP_KEY_STATUS )
		{

		}
		else
		{

		}
                
	}
}
void key_voldn_isr( void )
{
	if (EXTI_GetITStatus(KEY_EXTI_VOLDN_LINE) == SET)
	{
		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(KEY_EXTI_VOLDN_LINE);
		
		if( VOLDN_KEY_STATUS )
		{
			
		}
		else
		{
			
		}
		
	}
}
