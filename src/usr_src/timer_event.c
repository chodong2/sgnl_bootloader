#include "global_define.h"

void monitor_init(void)
{
	monitor_timer_config();
	monitor_timer_start();
}

/* Timer Configuration
* Use Timer 4
*/
void monitor_timer_config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the TIM4 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = MONITOR_TIM_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseInitTypeDef TIM_BaseStruct;
	/* TIM4 clock enable */
	RCC_APB1PeriphClockCmd(MONITOR_TIM_RCC, ENABLE);
	/* Time base configuration */
	TIM_BaseStruct.TIM_Period = 1600 - 1; // N (us)
	TIM_BaseStruct.TIM_Prescaler = 50; // 50 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(MONITOR_TIM_BASE, &TIM_BaseStruct);

	/* TIM IT enable */
	TIM_ITConfig(MONITOR_TIM_BASE, TIM_IT_Update, ENABLE);
}

void monitor_timer_stop(void)
{
	TIM_Cmd(MONITOR_TIM_BASE, DISABLE);
}

void monitor_timer_start(void)
{
	TIM_Cmd(MONITOR_TIM_BASE, ENABLE);
}

void monitor_timer_isr(void)
{
	if (TIM_GetITStatus(MONITOR_TIM_BASE, TIM_IT_Update) == SET)
	{
		TIM_ClearITPendingBit(MONITOR_TIM_BASE, TIM_IT_Update);
		
		g_str_timer.ui16_pwr_down_cnt++;

		if (g_str_bitflag.b1_power_key_flag)
		{
			if( POWER_KEY_STATUS )
			{
				g_str_timer.ui16_power_cnt++;
			}
			else
			{
				g_str_bitflag.b1_power_key_flag = 0;
			}

			if (g_str_timer.ui16_power_cnt > 300)
			{
				g_str_bitflag.b1_power_on = 1;
				g_str_timer.ui16_power_cnt = 0;
			}
		}

#ifdef ENABLE_PWM

#ifdef ENABLE_BATT_STATUS
		if( g_str_chg.ui16_chg_status == CHG_STATUS_IN_CHARGING )
		{
			//pattern_batt_level();
		}

#endif //ENABLE_BATT_STATUS
		
#endif //ENABLE_PWM
		
	}
}
