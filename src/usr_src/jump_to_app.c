#include "global_define.h"

typedef  void (*pFunction)(void);

pFunction Jump_To_Application;
uint32_t JumpAddress;

void JumpToUserApp(uint32_t jump_addr)
{
	/* Jump to user application */
	JumpAddress = *(__IO uint32_t*) (jump_addr + 4);
	Jump_To_Application = (pFunction) JumpAddress;
	Jump_To_Application();
}

/**
  * @brief  jump to application
  * @param  None
  * @retval None
  */
void jump_app( void )
{
	uint32_t jump_addr;

	jump_addr = *(__IO uint32_t*) (FLASH_ADDR_JUMP_APP);

	if((jump_addr != FLASH_ADDR_START_APP1) && (jump_addr != FLASH_ADDR_START_APP2))
	{
		if (((*(__IO uint32_t*)FLASH_ADDR_START_APP1) & 0x2FFE0000 ) == 0x20000000)
		{
			jump_addr = FLASH_ADDR_START_APP1;
		}
		else if(((*(__IO uint32_t*)FLASH_ADDR_START_APP2) & 0x2FFE0000 ) == 0x20000000)
		{
			jump_addr = FLASH_ADDR_START_APP2;
		}
		else /* Error of app start address */
		{
			while(1)
			{
			}
		}
	}
	JumpToUserApp(jump_addr);
}