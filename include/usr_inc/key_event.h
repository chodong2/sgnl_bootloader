#ifndef KEY_EVENT_H
#define KEY_EVENT_H

#define KEY_GPIO_VOL_PORT				GPIOB
#define KEY_GPIO_VOL_RCC				RCC_AHB1Periph_GPIOB

#define KEY_EXTI_VOL_PORT				EXTI_PortSourceGPIOB

#define KEY_GPIO_VOLUP_PIN				GPIO_Pin_3

#define KEY_EXTI_VOLUP_PIN				EXTI_PinSource3
#define KEY_EXTI_VOLUP_LINE				EXTI_Line3
#define KEY_EXTI_VOLUP_IRQ				EXTI3_IRQn

#define KEY_GPIO_VOLDN_PIN				GPIO_Pin_4

#define KEY_EXTI_VOLDN_PIN				EXTI_PinSource4
#define KEY_EXTI_VOLDN_LINE				EXTI_Line4
#define KEY_EXTI_VOLDN_IRQ				EXTI4_IRQn

#define KEY_GPIO_POWER_PORT				GPIOB
#define KEY_GPIO_POWER_PIN				GPIO_Pin_8
#define KEY_GPIO_POWER_RCC				RCC_AHB1Periph_GPIOB

#define KEY_EXTI_POWER_PORT				EXTI_PortSourceGPIOB
#define KEY_EXTI_POWER_PIN				EXTI_PinSource8
#define KEY_EXTI_POWER_LINE				EXTI_Line8
#define KEY_EXTI_POWER_IRQ				EXTI9_5_IRQn

void key_init( void );
void key_config_power( void );
void key_config_vol( void );

#define key_vol_up_isr					EXTI3_IRQHandler
#define key_vol_dn_isr					EXTI4_IRQHandler
#define key_power_isr					EXTI9_5_IRQHandler

#define POWER_KEY_STATUS				( KEY_GPIO_POWER_PORT->IDR & KEY_GPIO_POWER_PIN )

#define VOLUP_KEY_STATUS				( KEY_GPIO_VOL_PORT->IDR & KEY_GPIO_VOLUP_PIN )
#define VOLDN_KEY_STATUS				( KEY_GPIO_VOL_PORT->IDR & KEY_GPIO_VOLDN_PIN )

#endif //KEY_EVENT_H