#ifndef _GLOBAL_DEFINE_H_
#define _GLOBAL_DEFINE_H_

#include <stdio.h>
#include "stm32f4xx.h"

#include "pwm_event.h"
#include "uart_event.h"

#include "uart_parser.h"

#include "pin_event.h"
#include "key_event.h"
#include "timer_event.h"

#include "struct.h"

#include "jump_to_app.h"

extern void Delay(__IO uint32_t nTime);

#endif //_GLOBAL_DEFINE_H_