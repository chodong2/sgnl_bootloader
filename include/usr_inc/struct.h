#ifndef STRUCT_H
#define STRUCT_H

#include "global_define.h"

typedef struct bitflag_struct_{

	volatile uint16_t b1_frame_err:1;
	volatile uint16_t b1_power_key_flag:1;
	volatile uint16_t b1_power_on:1;
	volatile uint16_t b1_uart_end:1;

	volatile uint16_t b1_dimming_flag:1;

}bitflag_struct_t;

extern bitflag_struct_t g_str_bitflag;

typedef struct timer_struct_{

	volatile uint16_t ui16_audio_cnt;
	volatile uint16_t ui16_power_cnt;
	volatile uint16_t ui16_led_cnt;
	
	volatile uint16_t ui16_pwr_down_cnt;

}timer_struct_t;

extern timer_struct_t g_str_timer;

typedef struct bt_uart_struct_{
  
	volatile uint8_t	ui8_rx_buf[BT_UART_BUFFERSIZE];
	volatile uint16_t	ui16_rx_head;
	volatile uint16_t	ui16_rx_tail;
	volatile uint16_t	ui16_rx_cnt;
	volatile uint16_t	ui16_rx_flag;
	
	volatile uint8_t	ui8_tx_buf[BT_UART_BUFFERSIZE];
	volatile uint16_t	ui16_tx_head;
	volatile uint16_t	ui16_tx_tail;
	volatile uint16_t	ui16_tx_cnt;
	volatile uint16_t	ui16_tx_flag;
	
	volatile uint16_t ui16_batt_voltage;
  
}bt_uart_struct_t;

extern bt_uart_struct_t g_str_bt;

typedef struct led_pwm_struct_{
	volatile uint16_t ui16_led1_pulse;
}led_pwm_struct_t;

extern led_pwm_struct_t g_str_pwm;


typedef enum charger_status_{

	CHG_STATUS_NO_PLUG				= 0x00,
	CHG_STATUS_IN_CHARGING			= 0x01,
	CHG_STATUS_CHARGING_COMPLETE	= 0x02,
	CHG_STATUS_CHARGING_FAIL		= 0x03,
	CHG_STATUS_TYPE_REPORT			= 0x04,

	CHG_STATUS_INITIAL				= 0xFF,

}charger_status_t;

typedef enum charger_type_{

	CHG_TYPE_UNKNOWN	= 0x00,
	CHG_TYPE_NON_DCD	= 0x01,
	CHG_TYPE_SDP		= 0x02,
	CHG_TYPE_DCP		= 0x03,
	CHG_TYPE_CDP		= 0x04,
	CHG_TYPE_SONY		= 0x05,
	CHG_TYPE_APPLE_2_5W	= 0x06,
	CHG_TYPE_APPLE_5W	= 0x07,
	CHG_TYPE_APPLE_10W	= 0x08,
	CHG_TYPE_APPLE_12W	= 0x09,

	CHG_TYPE_INITIAL	= 0xFF,

}charger_type_t;

typedef struct chg_struct_{
	
	volatile charger_status_t ui16_chg_status;
	volatile charger_type_t ui16_chg_type;

}chg_struct_t;

extern chg_struct_t g_str_chg;

typedef enum battery_status_{

	BATT_STATUS_DANGER				= 0x00,
	BATT_STATUS_LOW					= 0x01,
	BATT_STATUS_NORMAL				= 0x02,
	BATT_STATUS_HIGH				= 0x03,
	BATT_STATUS_FULL				= 0x04,
	BATT_STATUS_IN_CHARGING			= 0x05,
	BATT_STATUS_CHARGING_COMPLETE	= 0x06,
	
	BATT_STATUS_INITIAL				= 0xFF,

}battery_status_t;

typedef enum battery_level_{

	BATT_LEVEL_3_0 = 0x00,
	BATT_LEVEL_3_1 = 0x01,
	BATT_LEVEL_3_2 = 0x02,
	BATT_LEVEL_3_3 = 0x03,
	BATT_LEVEL_3_4 = 0x04,
	BATT_LEVEL_3_5 = 0x05,
	BATT_LEVEL_3_6 = 0x06,
	BATT_LEVEL_3_7 = 0x07,
	BATT_LEVEL_3_8 = 0x08,
	BATT_LEVEL_3_9 = 0x09,
	BATT_LEVEL_4_0 = 0x0A,
	BATT_LEVEL_4_1 = 0x0B,
	BATT_LEVEL_4_2 = 0x0C,
	
	BATT_LEVEL_INITIAL = 0xFF,

}battery_level_t;

typedef struct batt_struct_{
	
	volatile battery_status_t ui16_batt_status;
	volatile battery_level_t ui16_batt_level;

}batt_struct_t;

extern batt_struct_t g_str_batt;

#endif //STRUCT_H
