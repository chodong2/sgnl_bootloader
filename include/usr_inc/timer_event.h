#ifndef TIMER_EVENT_H
#define TIMER_EVENT_H

#define MONITOR_TIM_IRQ     TIM4_IRQn
#define MONITOR_TIM_RCC     RCC_APB1Periph_TIM4
#define MONITOR_TIM_BASE    TIM4

void monitor_init( void );
void monitor_timer_config( void );
void monitor_timer_stop( void );
void monitor_timer_start( void );

#define monitor_timer_isr                  TIM4_IRQHandler

#endif //TIMER_EVENT_H