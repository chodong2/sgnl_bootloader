#ifndef _PIN_EVENT_H_
#define _PIN_EVENT_H_

void pin_enable_init(void);
void pin_enable_gpio(void);

#define PIN_ENABLE_POWER                GPIO_Pin_15

#define PIN_ENABLE_MFB					GPIO_Pin_5
#define PIN_ENABLE_BTRESET				GPIO_Pin_2

#define POWER_HOLD_ENABLE               GPIOA->BSRRL = PIN_ENABLE_POWER
#define POWER_HOLD_DISABLE              GPIOA->BSRRH = PIN_ENABLE_POWER

#define MFB_ENABLE						GPIOA->BSRRL = PIN_ENABLE_MFB
#define MFB_DISABLE						GPIOA->BSRRH = PIN_ENABLE_MFB

#define BTRESET_ENABLE					GPIOB->BSRRL = PIN_ENABLE_BTRESET
#define BTRESET_DISABLE					GPIOB->BSRRH = PIN_ENABLE_BTRESET

#endif //_PIN_EVENT_H_